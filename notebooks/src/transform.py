from datetime import datetime, time
import pandas as pd

def transform(_df: pd.DataFrame, start_time: time, end_time: time, aperture: float) -> pd.DataFrame:
    month_lt = {
        'января': 'january',
        'февраля': 'february',
        'марта': 'march',
        'апреля': 'april',
        'мая': 'may',
        'июня': 'june',
        'июля': 'july',
        'августа': 'august',
        'сентября': 'september',
        'октября': 'october',
        'ноября': 'november',
        'декабря': 'december',
    }

    def cvt_form_ru_datetime(dts: str) -> datetime:
        dts = dts.replace(' г. ', ' ').replace(' мсек', '').strip()
        dts = ' '.join(month_lt.get(x, x) for x in dts.split())
        return datetime.strptime(dts, "%d %B %Y %H:%M:%S.%f")
    
    _dt =  _df['Дата и время записи'].apply(lambda x: cvt_form_ru_datetime(x))
    _df.insert(2, 'dt', _dt)

    _mask = (_df['dt'].dt.time >= start_time) & (_df['dt'].dt.time <= end_time)
    _df = _df.loc[_mask]

    _df = _df.sort_values(by='dt')

    ndf = {}
    for param in _df.columns[3:]:
        ndf[f'{param}_diff'] = _df[param].diff().abs() > aperture
    
    _df = pd.concat([_df, pd.DataFrame(ndf)], axis=1)

    _df = _df.drop(columns=['dt'], axis=1)
    _df = _df.loc[_df.iloc[:, 2:].any(axis=1)]
    _df = _df.drop(_df.filter(regex='_diff').columns, axis=1)
    _df = _df.sort_values(by='RecordID')

    return _df
