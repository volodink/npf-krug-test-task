import pandas as pd


def load(_df: pd.DataFrame, filename: str, encoding: str = "utf-8") -> None:
    _df.to_csv(
        filename,
        encoding=encoding,
        sep=';',
        decimal=',',
        index=False
    )
