import pandas as pd
import polars as pl

def extract(filename: str, encoding: str = "utf-8") -> pd.DataFrame:  
    header = pd.read_csv(filename, encoding=encoding, delimiter=';', nrows=0).columns
    types_dict = {'RecordID': int, 'Дата и время записи': str}
    types_dict.update({col: float for col in header[2:]})

    _df = pd.read_csv(
        filename,
        encoding=encoding,
        delimiter=';',
        decimal=',',
        dtype=types_dict,
        converters={'RecordID': int, 'Дата и время записи': str.strip},
    )

    return _df
